const { expect } = require('chai');
const NumbersValidator = require('../../src/number_validator');

describe('isNumberEven tests', () => {
  const valid = new NumbersValidator();
  it('should return true when it is provided with an even number', () => {
    const validResult = valid.isNumberEven(4);
    expect(validResult).to.be.equal(true);
  });
  it('should return false when it is provided with an odd number', () => {
    const invalidResult = valid.isNumberEven(5);
    expect(invalidResult).to.be.equal(false);
  });
  it('should throw an error when it is provided a string', () => {
    const isString = '3';
    expect(() => {
      valid.isNumberEven(isString);
    }).to.throw('[3] is not of type "Number" it is of type "string"');
  });
});

describe('getEvenNumbersFromArray tests', () => {
  const evenArray = new NumbersValidator();
  it('should return an array of even numbers', () => {
    const arrayOfNum = [4, 8, 11, 12];
    const evenNumArray = evenArray.getEvenNumbersFromArray(arrayOfNum);
    expect(evenNumArray).to.be.eql([4, 8, 12]);
  });
  it('should throw an error if any element of the array is not a number', () => {
    const someArray = [6, 8, '1', 10, 24, '5'];
    expect(() => {
      evenArray.getEvenNumbersFromArray(someArray);
    }).to.throw('[6,8,1,10,24,5] is not an array of "Numbers"');
  });
});

describe('isAllNumbers tests', () => {
  const allNums = new NumbersValidator();
  it('should throw an error if it is provided a string', () => {
    const someString = '6';
    expect(() => {
      allNums.isAllNumbers(someString);
    }).to.throw('[6] is not an array');
  });
  it('should return array of numbers', () => {
    const validArrayNum = allNums.isAllNumbers([5, 7, 9, 11, 23]);
    expect(validArrayNum).to.be.equal(true);
  });
});

describe('isInteger tests', () => {
  const intNum = new NumbersValidator();
  it('should throw an error if the type of "n" is not a number', () => {
    const charValue = 'A';
    expect(() => {
      intNum.isInteger(charValue);
    }).to.throw('[A] is not a number');
  });

  it('should return true when it is provided with a number', () => {
    const trueNum = intNum.isInteger(5);
    expect(trueNum).to.be.equal(true);
  });
});
